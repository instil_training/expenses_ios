/*: lambdas; defining blocks; passing blocks as arguments;
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Protocols and Extensions
 */

import Foundation

//: ## Defining and implementing protocols
//: ### Basic Protocols
protocol Shape {
    var area: Double { get }
    var perimeter: Double { get }
}

protocol Sprite {
    var name: String { get set }
    
    func draw()
    
    init()
}

struct Circle : Shape, Sprite {
    var perimeter: Double = 0
    var area: Double = 1
    
    var name = ""
    
    func draw() {
        print("Drawing Cicle")
    }
}

var s = Circle() as Shape
print(s.area)
print(s.perimeter)

//: ### More Advanced Features
@objc protocol NumberSeries {
    @objc optional var name: String { get }
    
    var value: Double { get }
    
    init(seedValue: Double)
    
    static func create(seedValue: Double) -> NumberSeries
    
    func increment()
}

class ArithmeticSeries : NumberSeries {
    private var _value: Double
    
    var value: Double {
        return _value
    }
    
    required init(seedValue: Double) {
        _value = seedValue
    }
    
    static func create(seedValue: Double) -> NumberSeries {
        return ArithmeticSeries(seedValue: seedValue)
    }
    
    func increment() {
        _value += 1.0
    }
}


//: ## Extensions
extension Int {
    func isEven() -> Bool {
        return self % 2 == 0
    }
}

print(12.isEven())
print(13.isEven())

protocol Mover {
    func move(x: Double, y: Double)
}

extension Sprite {
    init(name: String) {
        self.init()
        self.name = name
    }
}

extension Circle : Mover {
    func move(x: Double, y: Double) {
        print("Moving by (\(x), \(y))")
    }
}

let c = Circle(name: "Face")
c.move(x: 5, y: 8)

let mover: Mover = c

//: [Previous](@previous) - [Next](@next)

