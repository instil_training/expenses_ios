/*: lambdas; defining blocks; passing blocks as arguments;
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Closures
 */

import Foundation

//: ## Higher Order Functions
//: ### Returning functions
func powers(of base: Int) -> (Int) -> Int {
    func raiseBase(to: Int) -> Int {
        return Int(pow(Double(base), Double(to)))
    }
    
    return raiseBase;
}
print(type(of: powers))

let powersOf2 = powers(of: 2)
print(powersOf2(8))

//: ### Functions as input and output
func getRepeater(transform: @escaping (String) -> String, times: Int) -> (String) -> String {
    func repeatedTransform(msg: String) -> String {
        var result = msg
        for _ in 0..<times {
            result = transform(result)
        }
        return result
    }

    return repeatedTransform;
}
print(type(of: getRepeater))

func addStars(title: String) -> String {
    return "*" + title + "*"
}

let add5Stars = getRepeater(transform: addStars, times: 5)
print(add5Stars("Functional Programming"))


//: ## Defining closures expressions
let numbers = [34, 23, 65, 3, 87, 12, 23, 52, 66]
var evenNumbers: [Int]

evenNumbers = numbers.filter(
    { (val: Int) -> Bool in val % 2 ==  0}
)
evenNumbers = numbers.filter(
    { val in val % 2 == 0 }
)
evenNumbers = numbers.filter(
    { $0 % 2 == 0 }
)

// Common to use single letter for parameters when obvious
print( numbers.map({x in x * 2}) )
print( numbers.map({x in String(x)}) )

//: ## Trailing closures
func every(_ numbers: [Int], test: (Int) -> Bool) -> Bool {
    for number in numbers {
        if !test(number) {
            return false
        }
    }
    return true
}

var areAllEven: Bool
areAllEven = every([2, 4, 8, 16], test: { x in x % 2 == 0 })
print(areAllEven)

areAllEven = every([2, 4, 7, 10]) { x in x % 2 == 0 }
print(areAllEven)

print( numbers.filter { x in x % 2 == 0 }
              .map { x in x * 2})

// Very useful with multi line closures
func iterateOver(_ items: [String], action: (String) -> ()) {
    for item in items {
        action(item)
    }
}

iterateOver(["Eamonn", "Bob", "Sally"]) { name in
    let message = "Hello \(name)"
    print(message)
}


func time(_ action: () -> ()) {
    let startTime = CFAbsoluteTimeGetCurrent()
    action()
    let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
    print("Time elapsed: \(timeElapsed) seconds")
}

time {
    let end = 10_000
    var result: Int64 = 0
    for number in 1...end {
        result += result
    }
    print("Sum of first \(end) numbers is \(result)")
}

//: ## Auto-closures
func multiply(_ a: Int, _ b: Int) -> Int {
    print("**** Multiplying values")
    return a * b
}

func deferEvaluation(_ input: @autoclosure () -> Int) {
    print("Entering function")
    print("Was passed in the value \(input())")
    print("Exiting function")
}

deferEvaluation(2)
deferEvaluation(multiply(10, 123))


//: ### Type aliases
typealias IntPredicate = (Int) -> Bool
var isEven4: IntPredicate = { $0 % 2 == 0 }
print(isEven4(15))

typealias StraightTransform<T> = (T) -> T

//: ## Operators as Functions
func mathApplier(_ a: Int, _ b: Int, operation: (Int, Int) -> Int) -> Int {
    return operation(a, b)
}

print(mathApplier(10, 3, operation:  +))
print(mathApplier(10, 3, operation:  -))
print(mathApplier(10, 3, operation:  *))
print(mathApplier(10, 3, operation:  /))

print( numbers.sorted(by: >) )

//: [Previous](@previous) - [Next](@next)
