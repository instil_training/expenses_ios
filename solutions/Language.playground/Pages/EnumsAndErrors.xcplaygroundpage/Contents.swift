/*: lambdas; defining blocks; passing blocks as arguments;
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Enums and Error handling
 */

//: ## Enums
enum DegreeUnit {
    case Celsius
    case Farenheit
}

var temperature = DegreeUnit.Celsius
print(temperature)
temperature = .Farenheit
print(temperature)

var waterBoilingPoint = 0
switch temperature {
case .Celsius:
    waterBoilingPoint = 100
case .Farenheit:
    waterBoilingPoint = 212
}
print("Water boils at \(waterBoilingPoint) \(temperature)")

//: ## Enums with associated values
enum Temperature {
    case Celsius(degrees: Int)
    case Fahrenheit(degrees: Int)
    case AbsoluteZero
}

var waterBoilsAt = Temperature.Celsius(degrees: 100)
print(waterBoilsAt)

let t1 = Temperature.Celsius(degrees: 451)
switch t1 {
case .Celsius(let degrees) where degrees > 100:
    print("Warmer than boiling water")
case .Celsius(let degrees) where degrees < 0:
    print("Colder than freezing")
case .Fahrenheit(let degrees) where degrees > 212:
    print("Warmer than boiling water")
case .Fahrenheit(let degrees) where degrees < 32:
    print("Colder than freezing")
case let temperature:
    print("Temperature is \(temperature)")
}

//: ## Raw Value Enums
enum BoilingPoint : Int {
    case Water = 100
    case Petrol = 95
    case Alcohol = 97
}

var t2 = BoilingPoint.Water
print("Raw Value = \(t2.rawValue)")

var t3 = BoilingPoint(rawValue: 97)
print("Material = ", t3 ?? "Unknown")

//: ## Defining errors
enum ServiceError : Error {
    case notAuthenticated
    case resourceError
    case generalError(code: Int)
}

//: ## Throwing errors
func getData(shouldThrow: Bool) throws -> Int {
    print("Start")

    if shouldThrow {
        throw ServiceError.generalError(code: 401)
    }
    
    print("End")
    return 42
}

//: ## Executing Throwable Code
let result1 = try getData(shouldThrow: false)
let result2 = try? getData(shouldThrow: false)
let result3 = try! getData(shouldThrow: false)

print("1: Value[ \(result1) ]  Type[ \(type(of: result1)) ]")
print("2: Value[ \(result2) ]  Type[ \(type(of: result2)) ]")
print("3: Value[ \(result3) ]  Type[ \(type(of: result3)) ]")

//: ### Catching Errors
do {
    var result = try getData(shouldThrow: true)
    print("1: Value[ \(result) ]  Type[ \(type(of: result)) ]")
} catch {
    print("Exception thrown")
}

do {
    var result = try? getData(shouldThrow: true)
    print("2: Value[ \(result) ]  Type[ \(type(of: result)) ]")
} catch ServiceError.generalError {
    print("Exception thrown")
}

do {
    var result = try! getData(shouldThrow: false)
    print("3: Value[ \(result) ]  Type[ \(type(of: result)) ]")
} catch ServiceError.generalError(let code)
    where code > 400 {
    print("Exception thrown - \(code)")
}

//: ## Defer
print("\n--- Defer ---")
do {
    print("Open file")
    defer {
        print("Close file")
    }
    
    print("Connect")
    defer {
        print("Disconnect")
    }
    
    print("Grab lock")
    throw ServiceError.resourceError
    defer {
        print("Release lock")
    }
} catch {
    print("Exception")
}

//: ## Assertions
var x = 5
assert(x > 0, "x should be greater than 0")


//: [Previous](@previous) - [Next](@next)
