/*: defining classes; initialising instances; structs; enums
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Classes
 */

import Foundation

//: ## Defining classes and structures
printHeader("Classes and Structs")

class Person {
    var firstname: String
    var lastname: String
    var age: Int
    
    init(firstname: String, lastname:String, age: Int) {
        self.firstname = firstname
        self.lastname = lastname
        self.age = age
    }
    
    func fullname() -> String {
        return "\(lastname), \(firstname)"
    }
}

var batman = Person(firstname: "Bruce", lastname: "Wayne", age: 35)
print(batman.fullname())
print(batman.age)


struct PersonStructure {
    var firstname: String
    var lastname: String
    var age: Int
    
    // This is actually optional
    // Stuctures will have a default member-wise initialiser
    init(firstname: String, lastname:String, age: Int) {
        self.firstname = firstname
        self.lastname = lastname
        self.age = age
    }
    
    func fullname() -> String {
        return "\(lastname), \(firstname)"
    }
}

var butler = PersonStructure(firstname: "Alfred", lastname: "Pennyworth", age: 70)
print(butler.fullname())
print(butler.age)


//: ## Copy by value or reference
printHeader("Copy by value and reference")
var copyClass = batman
copyClass.age = 99
print(batman.age, "Mutated")
print(batman === copyClass)

var copyStructure = butler
copyStructure.age = 99
print(butler.age, "Unchanged")

//: ## Properties
class ImmutableRectangle {
    let width: Int
    let height: Int
    
    var area: Int {
        print("Getting area")
        return width * height
    }
    
    var largerDimension: String {
        get {
            if width == height {
                return "Same"
            }
            
            return width > height ? "Width" : "Height"
        }
    }
   
    init(width: Int, height: Int) {
        print("Initialising Rectangle")
        self.width = width
        self.height = height
    }
    
    lazy var perimeter: Int = self.getPerimeter()
    
    func getPerimeter() -> Int {
        print("Calculating perimeter")
        return (width + height) * 2
    }
}

let rect = ImmutableRectangle(width: 10, height: 8)
print("Width = ", rect.width)
print("Height = ", rect.height)

print("Area = ", rect.area)
print("Area = ", rect.area)
print("Perimeter = ", rect.perimeter)
print("Perimeter = ", rect.perimeter)
print("Larger dimension = ", rect.largerDimension)

class Square {
    var length: Int
    static let numberOfSides = 4
    
    var area: Int {
        get {
            print("Getting area")
            return length * length
        }
        
        set {
            print("Setting area")
            length = Int(sqrt(Double(newValue)))
        }
    }
    
    init(length: Int) {
        print("Initialising Square")
        self.length = length
    }
    
    lazy var perimeter: Int = self.getPerimeter()
    
    func getPerimeter() -> Int {
        print("Calculating perimeter")
        return length * 4
    }
    
    static func getPerimeter(forLength length: Int) -> Int {
        return length * numberOfSides
    }
}

var square = Square(length: 8)
print("Length = ", square.length)
square.area = 100
print("Length = ", square.length)
print("Number of sides =", Square.numberOfSides)
print("Perimeter =", Square.getPerimeter(forLength: 12))

//: ## Property Observers
class Counter {
    var value = 0;
    
    var counter: Int = 0 {
        willSet {
            print("Going to update \(newValue)")
        }
        
        didSet {
            print("Updated \(oldValue)")
        }
    }
}

let counter = Counter()
counter.counter = 12
counter.counter = 23
print(counter.counter)
print(counter.counter)
print(counter.counter)


//: ## Subscripts
print("---- Times Tables")
class TimesTables {
    subscript(lhs: Int, rhs: Int) -> String {
        get {
            return "\(lhs) x \(rhs) = \(lhs * rhs)"
        }
    }
}

let timesTables = TimesTables()
print(timesTables[4, 1])
print(timesTables[4, 2])
print(timesTables[4, 3])


//: ## Extending existing classes
class Employee : Person {
    var jobTitle: String
    
    init(firstname: String, lastname:String, age: Int, jobTitle: String) {
        self.jobTitle = jobTitle
        super.init(firstname: firstname, lastname: lastname, age: age)
    }
    
    override convenience init(firstname: String, lastname:String, age: Int) {
        self.init(firstname: firstname, lastname: lastname,
                  age: age, jobTitle: "Graduate")
    }
    
    override func fullname() -> String {
        return super.fullname() + " [\(self.jobTitle)]"
    }
}

var me = Employee(firstname: "Eamonn", lastname: "Boyle", age: 36, jobTitle: "Captain")
print(me.fullname())

class Manager : Employee {
    init(firstname: String, lastname:String, age: Int) {
        super.init(firstname: firstname, lastname: lastname, age: age, jobTitle: "Manager")
    }
}

var bob = Manager(firstname: "Bob", lastname: "Bobson", age: 40)
print(bob.fullname())

//: ## Failable Initialisers
class Point : CustomStringConvertible {
    let x: Int
    let y: Int
    
    init?(coordinates: String) {
        let subParts = coordinates.components(separatedBy: " ")
        
        guard subParts.count == 2,
              let x = Int(subParts[0]),
              let y = Int(subParts[1]) else {
            return nil
        }
        
        self.x = x
        self.y = y
    }
    
    var description: String {
        return "(\(x), \(y))"
    }
}

let point1 = Point(coordinates: "12 24")
let point2 = Point(coordinates: "No point")

//: Type Checks and Casts
var myManager = Manager(firstname: "Bob", lastname: "Bobson", age: 50)

print(myManager is Manager)
print(myManager is Person)

// Up cast is fine
let managerPerson1: Person = myManager  // Implicit
let managerPerson2 = myManager as Person // Explicit

// Downcast must be explicit
// The following will not compile
//   var myManager2: Manager = managerPerson

let myManager3 = managerPerson1 as? Manager
print(type(of: myManager3)) // Optional<Manager>

let myManager4 = managerPerson1 as! Manager
print(type(of: myManager4)) // Manager
//: [Previous](@previous) - [Next](@next)
