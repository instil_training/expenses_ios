/*:
 ![Instil Logo](instil-logo-header.png)
 ___
 # The Swift Language
 
 ## Table of Contents

 * [Basic Structure](Structure)
 * [Variables](Variables)
 * [Control Flow](Control)
 * [Optionals](Optionals)
 * [Functions](Functions)
 * [Closures](Closures)
 * [Classes](Classes)
 * [Protocols and Extensions](Protocols)
 * [Error handling](EnumsAndErrors)
 * [Odds and Ends](OddsAndEnds)
 */
