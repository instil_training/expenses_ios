/*: Control Flow
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Control Flow
 */
import Foundation

//: ## If Block
printHeader("If Block")
var pi = 3.142, e = 2.718

if pi > e {
    print("pi is greater than e")
}
else {
    print("e is greater than pi")
}

var value = arc4random_uniform(75)
if value < 25 {
    print("Value is less than 25")
}
else if value < 50 {
    print("Value is between 25 and 49")
}
else {
    print("Value is greater or equal to 50")
}

//: ## For loop
printHeader("Loops")
for number in [10.0, 1000, 10000, 10000] {
    print(log10(number))
}

printHeader("Closed Range")
for number in 10...15 {
    print(number)
}

printHeader("Half Open Range")
for _ in 0..<5 {
    print("Repetition is the key to success")
}

printHeader("Dictionary Iteration")
var cast = [
    "Neo" : "Keanu Reeves",
    "Morpheus" : "Lawrence Fishburne",
    "Trinity" : "Carrie-Anne Moss",
]

for (character, actor) in cast {
    print("\(character) is played by \(actor)")
}

//: ## While Loop
printHeader("While & Repeat While - Fibonacci")

var fibonacci = [Int]()
var current = 1, previous = 0
while current < 100 {
    fibonacci.append(current)
    let temp = current
    current += previous
    previous = temp
}
print(fibonacci)

//: ## Repeat While
fibonacci = [Int]()
repeat {
    fibonacci.append(current)
    let temp = current
    current += previous
    previous = temp
} while fibonacci.count < 10;
print(fibonacci)

//: ## Switch Statements
printHeader("Switch Statements")
let letter: String = "o"
let type: String
switch letter {
    case " ":
        type = "Whitespace"
    case "a", "e", "i", "o", "u":
        type = "Vowel"
    case let x where "bcdfghjklmnpqrstvwxyz".contains(x):
        type = "Consonant"
    default:
        type = "Unknown letter"
}
print(type)

var letterTuple = ("e", "Vowel")
switch letterTuple {
    case let (letter, "Vowel"):
        print("Processing \(letter) as a Vowel")
    case let (letter, "Consonant"):
        print("Processing \(letter) as a Consonant")
    case let (_, type):
        print("I'm not familliar with \(type)")
}

//: [Next](@next)
