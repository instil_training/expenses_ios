/*: basic structure
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Basic Structure
 */

//: ## Statements
var x = 1
var y = 2;
var a = 3; var b = 4; var c = 5;
if true {
    print("This is a block")
    print("With multiple lines")
}

//: ## Comments
// This is a single line comment
print("") // They can follow statements
/* This is a another way */
/*
 It can be used to write
 multi-line comments 
 
    /* They even respect
       nesting */
 */

//: ## Functions with parameter names
print(1, 2, 3, 4, separator: "-", terminator: "!!!")

//: [Next](@next)
