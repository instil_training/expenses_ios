/*: declaring variables; type inference
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Variables
 */

//: ## Types - Inferred and Explicit
printHeader("Basic Types")
var myInt = 123
var myDouble = 456.0
var myFloat : Float = 78.9 // Explicit
var myBool = true
var myString = "Hello"
var myArray = [123, 456, 789]     // Array<Int> or [Int]
var myTuple = ("abc", 123, 456.0)
var myTupleWithNames = (name: "Eamonn", job: "Trainer")
var myDictionary = [              // Dictionary<string, string> or [String : String]
    "Neo" : "Keanu Reeves",
    "Morpheus" : "Lawrence Fishburne",
    "Trinity" : "Carrie-Anne Moss",
]
var mySet: Set<Int> = [1, 2, 4, 8, 16]

print(type(of: myInt))
print(type(of: myDouble))
print(type(of: myFloat))
print(type(of: myBool))
print(type(of: myString))
print(type(of: myArray))
print(type(of: myTuple))
print(type(of: myTupleWithNames))
print(type(of: myDictionary))
print(type(of: mySet))


// Using different bases and _ separators
printHeader("Literals")
var millions: Int = 1_500_006, x: Double = 1
var binary = 0b0010_1010, octal = 0o52, hex = 0x002A
print(binary, octal, hex, separator: " - ")

//: ## Unicode Naming
var 🍿🤗💩 = "Never do this"
var クレイジーネーム = 123
🍿🤗💩 += "...no seriously. Never do this!"


//: ## Basic Operations
printHeader("Basic Operations")
myInt = myInt * 2
myDouble *= 2
myString = myString.lowercased() + "world";
myBool = (true && false) || (!myBool)
myString = "myInt has the value \(myInt) and myDouble has \(myDouble)"

//: ### Collection Operations
myArray.append(999) // mutation
myArray.append(contentsOf: [1, 2, 3, 4]) // mutation
print(myArray.count)

let even = myArray.filter {$0 % 2 == 0 }
let odd = myArray.filter {$0 % 2 == 1 }
print(even + odd)

myDictionary["Agent Smith"] = "Hugo Weaving"

//: ### Decompose Tuple 
// _ for ignporing parts
var (part1, part2, _) = myTuple
var part3 = myTuple.2
print(part2)

//: ## Constants vs Variables
var myVariable = "Some message"
myVariable += " - Reassigned to new string"

var age = 36
age+=12

let myConstant = "Some message"
// This is illegal
// myConstant += "Test"
print(type(of: myConstant))

let myConstantArray = [1, 2, 3, 4]
// This is illegal - collections also immutable
// myConstantArray.append(999)
print(type(of: myConstantArray))

//: ## Casting
let anotherInt = 23

// This will not work
//     let myDouble = anotherInt
// Must be explicit
let aThirdDouble = Double(anotherInt)

print(type(of: aThirdDouble))

//: ## Ranges
var closedRange = 100...1000
var halfOpenRange = 0..<100
print(type(of: closedRange))
print(closedRange.overlaps(90...110))
print(closedRange.overlaps(halfOpenRange))

//: [Previous](@previous) - [Next](@next)
