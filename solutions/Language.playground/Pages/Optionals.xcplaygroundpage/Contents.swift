/*: optional types; unwrapping optionals; guards
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Optionals
 */

//: ## Declaring optional types
printHeader("Optionals Intro")
var myOptionalString: String? = "Eamonn"
var myOptionalInt: Int? = 4
var myOptionalArray : Array<Int>? = [1, 2, 3]
var myOptionalBool: Bool? = nil

print(type(of: myOptionalString))

// These are illegal because the variables may be nil
//   myOptionalInt += 2
//   myOptionalInt.append(6)

var intFromString = Int("42")
print(type(of: intFromString))

var myNonOptionalInt = 3
// This is also illegal- non-optional can never be nil
//   myNonOptionalInt = nil

//: ## Unwrapping
printHeader("Unwrapping")
//: ### Forced Unwrapping
myOptionalInt = myOptionalInt! + 2
myOptionalArray!.append(999)
print(myOptionalArray![2])

// Force unwrapping nill will cause runtime error
//   print(myOptionalBool!)
//: ### Nil-coalescing
let username: String? = nil
if username != nil {
    print(username!)
}
else {
    print("Unknown user")
}

print(username != nil ? username! : "Unknown user")

print(username ?? "Unknown user")

//: ### Optional Binding
if let extractedValue = intFromString {
    print(extractedValue) // No forced unwrapping
} else {
    print("Should never get here")
}

if let extractedBool = myOptionalBool {
    print("Should never get here!! ", extractedBool)
} else {
    print("myOptionalBool is nil")
}

let options = [2, 3, 5, 7, nil]
var index = 0;
while let value = options[index] {
    index += 1
    print(value)
}

if let ev1 = myOptionalString,
    let ev2 = myOptionalArray,
    let ev3 = myOptionalInt {
    print("Multiple bindings in a single block")
}
// Note, that the assigned entites exist only within
//   the if block. This is illegal
// print( ev1 )

//: ### Guards
func print(name: String?) {
    guard let myName = name  else {
        return
    }
    
    print(myName.uppercased())
}

print(name: "Eamonn")
print(name: nil)


//: ### Optional Chaining
myOptionalArray?.append(999)
let result = myOptionalArray?[3].description.isEmpty
print(type(of: result))
myOptionalArray?[3] = 100

//: ## Binding Behaviour
printHeader("Binding - Value or Reference")
class SimpleReferenceType {
    var prop = 12
}

let source: SimpleReferenceType? = SimpleReferenceType()
if var local = source {
    local.prop = 12345 // Source IS mutated
}
print("Optional Binding Behaviour = \(source!.prop)")


func guardBindingBehaviour(_ source: SimpleReferenceType?) {
    guard var localFromGuard = source else {
        return
    }
    localFromGuard.prop = 67890  // Source IS mutated
}
guardBindingBehaviour(source)
print("Guard behaviour = \(source!.prop)")


if var variableIntArray = myOptionalArray {
    variableIntArray.append(42) // Source object is NOT mutated - value type
}
print(myOptionalArray!)

//: ## Implicit Optionals
printHeader("Implicit Optionals")
var myImplicitOptional: Int! = myOptionalInt
myImplicitOptional = myImplicitOptional + 2
if let value = myImplicitOptional {
    print("Implicit optionals can still be bound")
}

myImplicitOptional = nil

print(type(of: myImplicitOptional))

//: [Previous](@previous) - [Next](@next)
