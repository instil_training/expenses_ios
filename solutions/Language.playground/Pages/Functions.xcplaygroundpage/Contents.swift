/*: defining and calling functions
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Functions
 */

import Foundation

//: ## Defining functions

func add(lhs: Int, rhs: Int) -> Int {
    return lhs + rhs
}
var result = add(lhs: 45, rhs: 55)

 // Void function
func printHello() {
    print("Hello")
}
printHello()

// Passing arguments
func printMessage(message: String) -> () {
    print(message)
}
printMessage(message: "Test")

// Returning data
func lowerCase(this: String) -> String {
    return this.lowercased()
}
print(lowerCase(this: "this was UPPER"))

func divide(numerator: Int, denominator: Int) -> (Int, Int)? {
    guard denominator != 0 else {
        return nil
    }
    
    return (numerator / denominator, numerator % denominator)
}
print(divide(numerator: 37459, denominator: 1234)!)

printHeader("Function Types")
print(type(of: printHello))
print(type(of: printMessage))
print(type(of: lowerCase))
print(type(of: divide))



//: ## Argument labels
printHeader("Argument labels")
func upperCase(thisString str: String) -> String {
    return str.uppercased()
}
print(upperCase(thisString: "this was lower"))

// Avoid caller specifying the argument name
func surround(_ str: String, with padding: String) -> String {
    return padding + str + padding
}
print(surround("This string is padded", with: "**"))


//: ## Overloads via Labels
func transform(toUpper str: String) -> String {
    return str.uppercased()
}

func transform(toLower str: String) -> String {
    return str.lowercased()
}

print(transform(toUpper: "aBcDeF"))
print(transform(toLower: "aBcDeF"))

//: ## Default arguments
func getLog(_ number: Double, forBase base: Int = 10) -> Double {
    switch base {
    case 2:
        return log2(number)
    case 10:
        return log10(number)
    default:
        return 0
    }
}
print(getLog(1000))
print(getLog(64, forBase: 2))

//: ## Variadic Functions
func sum(_ numbers: Int...) -> Int {
    var total = 0
    for number in numbers {
        total += number
    }
    return total
}
print(sum(45, 21, 28, 74, 56))

//: ## Inout Arguments
func swap(_ a: inout String, _ b: inout String) {
    let temp = a;
    a = b
    b = temp
}

var first = "To me, "
var second = "to you"
swap(&first, &second)
print(first, second)

// Reference types can always be mutated
class SimpleReferenceType {
    var prop = 12
}

func mutateObject(input: SimpleReferenceType) {
    input.prop = 99
}

let myObject = SimpleReferenceType()
mutateObject(input: myObject)
print(myObject.prop)

//: ## Nested Functions
func CollatzSequence(start: Int) -> [Int] {
    // This variable must be before the function
    var sequence: [Int] = []
    
    func even(_ input: Int) -> Int{
        return input / 2
    }
    
    func odd(_ input: Int) -> Int {
        return (input * 3) + 1
    }
    
    func populate(_ input: Int) {
        sequence.append(input)
        switch input {
        case 1:  return
        case let x where x % 2 == 0:
            populate(even(input))
        default:
            populate(odd(input))
        }
    }
    
    // The function usage must be after definition
    populate(start)
    return sequence
}

print(CollatzSequence(start: 12))

//: [Previous](@previous) - [Next](@next)
