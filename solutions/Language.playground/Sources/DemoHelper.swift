import Foundation


public func printHeader(_ header: String) {
    print()
    print("--- \(header) ---")
}
