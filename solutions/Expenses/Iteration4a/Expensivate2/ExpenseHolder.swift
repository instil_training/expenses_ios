import Foundation

protocol ExpenseHolder {
    var expense: Expense? { get set }
}
