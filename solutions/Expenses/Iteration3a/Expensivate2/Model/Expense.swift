import Foundation

struct Expense: CustomStringConvertible {
    var title: String
    var date: String
    var cost: Double
    
    var description: String {
        return "\(title) - \(cost)"
    }
}
