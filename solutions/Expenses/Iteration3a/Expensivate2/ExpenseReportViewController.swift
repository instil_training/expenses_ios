import UIKit

class ExpenseReportViewController: UIViewController, ExpenseReportHolder, UITableViewDataSource {

    @IBOutlet weak var titleText: UITextField!
    
    var expenseReport: ExpenseReport?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshView()
    }

    private func refreshView() {
        titleText.text = expenseReport?.title ?? ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenseReport?.expenses.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        assert(expenseReport != nil, "Should never get rows if report is nil")
        
        let cell = UITableViewCell()
        cell.textLabel?.text = String(describing: expenseReport!.expenses[indexPath.row])
        return cell
    }
}
