import Foundation

class ExpenseReportRepository {
    private var reports: [ExpenseReport] = []
    
    init() {
        let er1 = ExpenseReport(title: "Trip to Turkey")
        er1.expenses.append(Expense(title: "Food", date: "2017-06-01", cost: 15.50))
        let er2 = ExpenseReport(title: "Trip to Dublin")
        er2.expenses.append(Expense(title: "Taxi", date: "2017-06-02", cost: 10.00))
        er2.expenses.append(Expense(title: "Snack", date: "2017-06-06", cost: 8.00))
        
        reports.append(er1)
        reports.append(er2)
    }
    
    var count: Int {
        return reports.count
    }
    
    subscript(_ index: Int) -> ExpenseReport {
        get {
            return reports[index]
        }
    }
    
    func addOrUpdate(_ expenseReport: ExpenseReport?) {
        guard let reportToAdd = expenseReport else {
            return // Simply ignore nil
        }
        
        addIfNotPresent(reportToAdd)
        // No update require as using reference
    }
    
    func addIfNotPresent(_ reportToAdd: ExpenseReport) {
        let report = reports.first(where: { $0 === reportToAdd })
        if report == nil {
            reports.append(reportToAdd)
        }
    }
}
