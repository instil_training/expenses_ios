import UIKit

class ExpenseReportViewController: UIViewController,
                                   ExpenseReportHolder,
                                   UITableViewDataSource,
                                   UITableViewDelegate,
                                   Saver {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var costLabel: UILabel!
    
    var expenseReport: ExpenseReport?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshView()
    }

    func saveChanges() {
        if let report = expenseReport {
            report.title = titleText.text ?? report.title
        }
    }
    
    private func refreshView() {
        titleText.text = expenseReport?.title ?? ""
        costLabel.text = "\(expenseReport?.totalCost ?? 0.0)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenseReport?.expenses.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        assert(expenseReport != nil, "Should never get rows if report is nil")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "expenseCell", for: indexPath) as! ExpenseTableViewCell
        let expense = expenseReport!.expenses[indexPath.row]
        cell.expense = expense
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard var destination = segue.destination as? ExpenseHolder,
            let sender = sender as? ExpenseHolder,
            let identifier = segue.identifier else {
                return
        }
        
        assert(identifier == "editExpense")
        
        destination.expense = sender.expense
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @IBAction func saveChanges(_ segue: UIStoryboardSegue) {
        guard let source = segue.source as? Saver else {
            print("Returned from non-Saver")
            return
        }
        print("Saving expense changes")
        source.saveChanges()
        tableView.reloadData()
    }
    
    @IBAction func cancel(_ segue: UIStoryboardSegue) {
        print("Cancelling")        
    }
}
