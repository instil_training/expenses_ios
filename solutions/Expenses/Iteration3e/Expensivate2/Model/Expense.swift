import Foundation

class Expense: CustomStringConvertible {
    var title: String
    var date: String
    var cost: Double
    
    init(title: String, date: String, cost: Double) {
        self.title = title
        self.date = date
        self.cost = cost
    }
    
    var description: String {
        return "\(title) - \(cost)"
    }
}
