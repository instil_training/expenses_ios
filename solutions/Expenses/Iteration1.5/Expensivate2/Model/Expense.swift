import Foundation

struct Expense {
    var title: String
    var date: String
    var cost: Double
}
