import Foundation

class ExpenseReport : CustomStringConvertible {
    var expenses: [Expense] = []
    
    var date: String
    var title: String
    
    init(title: String) {
        self.title = title
        self.date = ""
    }
    
    var description: String {
        return "\(title) (\(expenses.count) expenses)"
    }
    
    var totalCost: Double {
        return expenses.reduce(0.0, { $0 + $1.cost })
    }
}
