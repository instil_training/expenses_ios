import Foundation

protocol ExpenseReportHolder {
    var expenseReport: ExpenseReport? { get set }
}
