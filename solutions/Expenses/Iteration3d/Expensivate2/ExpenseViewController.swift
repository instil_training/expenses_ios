import UIKit

class ExpenseViewController: UIViewController, ExpenseHolder, Saver {

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var costText: UITextField!
    @IBOutlet weak var dateText: UITextField!
    
    var expense: Expense?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func refreshView() {
        if let currentExpense = expense {
            titleText.text = currentExpense.title
            costText.text = "£\(currentExpense.cost)"
            dateText.text = currentExpense.date
        }
    }
    
    func saveChanges() {
        guard let currentExpense = expense,
              let cost = parseMoney(costText.text ?? "") else {
            return
        }
        
        currentExpense.title = titleText.text ?? currentExpense.title
        currentExpense.cost = cost
        currentExpense.date = dateText.text ?? currentExpense.date
    }
    
    func parseMoney(_ money: String) -> Double? {
        let moneyCharacters = CharacterSet(charactersIn: " £:")
        let strippedMoney = money.trimmingCharacters(in: moneyCharacters)
        
        if let value = Double(strippedMoney) {
            return value
        }
        
        return nil
    }
}
