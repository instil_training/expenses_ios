# Expenses solution

These solutions show a gradual progression towards the final solution.

The final solution is as follows:

- Add, Edit and Remove expense reports
- Add, Edit and Remove expenses within a report
- Persistance using Realm
- Takes a picture in epxense and sends to server to parse price
  - This uses the ImageServer demo in the examples folder
  - It is a Vapor project (see its readme)

The later projects use 'Carthage' to manage packages, not Cocoa Pods.
