//
//  Expense.swift
//  Expensivate2
//
//  Created by Eamonn Boyle on 04/07/2017.
//  Copyright © 2017 Eamonn Boyle. All rights reserved.
//

import Foundation

struct Expense : CustomStringConvertible {
    var title: String
    var date: String
    var cost: Double
    
    var description: String {      
        return "\(title) - \(date)"
    }
}
