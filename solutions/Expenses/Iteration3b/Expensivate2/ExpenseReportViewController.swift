import UIKit

class ExpenseReportViewController: UIViewController, ExpenseReportHolder, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var titleText: UITextField!
    
    var expenseReport: ExpenseReport?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshView()
    }

    private func refreshView() {
        titleText.text = expenseReport?.title ?? ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenseReport?.expenses.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        assert(expenseReport != nil, "Should never get rows if report is nil")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "expenseCell", for: indexPath) as! ExpenseTableViewCell
        let expense = expenseReport!.expenses[indexPath.row]
        cell.expense = expense
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("Getting cell height")
        return 60
    }
}
