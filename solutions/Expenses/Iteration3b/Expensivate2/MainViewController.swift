import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let repository = ExpenseReportRepository()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "expenseReportCell", for: indexPath) as! ExpenseReportTableViewCell
        cell.expenseReport = repository[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repository.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard var destination = segue.destination as? ExpenseReportHolder,
              let sender = sender as? ExpenseReportHolder,
              let identifier = segue.identifier else {
            return
        }
        
        assert(identifier == "editExpenseReport")
        
        destination.expenseReport = sender.expenseReport
    }
    
}

