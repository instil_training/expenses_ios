import UIKit
import Alamofire
import AlamofireImage

class ExpenseViewController: UIViewController,
                             ExpenseHolder, Saver,
                             UIImagePickerControllerDelegate,
                             UINavigationControllerDelegate {

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var costText: UITextField!
    @IBOutlet weak var dateText: UITextField!
    @IBOutlet weak var receiptImageView: UIImageView!
    
    private var picker = UIImagePickerController()
    
    var expense: Expense?
    
    @IBAction func takePicture(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            picker.cameraCaptureMode = .photo
        }
        else {
            picker.sourceType = .photoLibrary
        }
        
        self.present(picker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshView()
        picker.delegate = self
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            receiptImageView.image = image
        }
        
        dismiss(animated: true, completion: nil)
        getPrice()
    }
    
    private func getPrice() {
        guard let expense = expense,
              let image = receiptImageView.image,
              let imageData = UIImageJPEGRepresentation(image,  0.9) else {
            return
        }
        
        var imagePath = URL(string: "http://localhost:8080/")!
        imagePath.appendPathComponent("getprice")
        imagePath.appendPathComponent(expense.id)
        
        Alamofire.upload(imageData, to: imagePath)
            .responseJSON { response in
                if let priceJSON = response.result.value as? [String: Any?],
                   let price = priceJSON["price"] as? Double {
                    self.costText.text = "£\(price)"
                }
            }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func refreshView() {
        if let currentExpense = expense {
            titleText.text = currentExpense.title
            costText.text = "£\(currentExpense.cost)"
            dateText.text = currentExpense.date
        }
    }
    
    func saveChanges() {
        guard let currentExpense = expense,
              let cost = parseMoney(costText.text ?? "") else {
            return
        }
        
        currentExpense.update {
            currentExpense.title = titleText.text ?? currentExpense.title
            currentExpense.cost = cost
            currentExpense.date = dateText.text ?? currentExpense.date
        }
    }
    
    func parseMoney(_ money: String) -> Double? {
        let moneyCharacters = CharacterSet(charactersIn: " £:")
        let strippedMoney = money.trimmingCharacters(in: moneyCharacters)
        
        if let value = Double(strippedMoney) {
            return value
        }
        
        return nil
    }
}
