import Foundation
import RealmSwift

class ExpenseReport : Object {
    let expenses = List<Expense>()
    dynamic var date = ""
    dynamic var title = ""
    dynamic var id = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override var description: String {
        return "\(title) (\(expenses.count) expenses)"
    }
    
    var totalCost: Double {
        return expenses.reduce(0.0, { $0 + $1.cost })
    }
    
    func addOrUpdate(_ expense: Expense?) {
        guard let expenseToAdd = expense else {
            return // Simply ignore nil
        }
        
        self.update {
            addIfNotPresent(expenseToAdd)
        }
        // No update require as using reference
    }
    
    func addIfNotPresent(_ expenseToAdd: Expense) {
        let expense = expenses.first(where: { $0.id == expenseToAdd.id })
        if expense == nil {
            expenses.append(expenseToAdd)
        }
    }
    
    func delete(_ expense: Expense?) {
        guard let expenseToDelete = expense else {
            return
        }
        
        self.update {
            if let index = expenses.index(where: { $0.id == expenseToDelete.id }) {
                expenses.remove(objectAtIndex: index)
            }
        }
    }
}

extension ExpenseReport {
    func with(title: String) -> ExpenseReport {
        self.title = title
        return self
    }
}

