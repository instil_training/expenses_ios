import Foundation
import RealmSwift

class ExpenseReportRepository : Object {
    
    let reports = List<ExpenseReport>()
    
    func populateWithDummyData() {
        let er1 = ExpenseReport().with(title: "Trip to Turkey")
        
        er1.expenses.append(objectsIn: [
            Expense().with(title: "Food").with(date: "2017-06-01").with(cost: 15.50)
        ])
        
        let er2 = ExpenseReport().with(title: "Trip to Dublin")
        er2.expenses.append(objectsIn: [
            Expense().with(title: "Taxi").with(date: "2017-06-02").with(cost: 10.00),
            Expense().with(title: "Snack").with(date: "2017-06-06").with(cost: 8.00)
        ])
        
        reports.append(er1)
        reports.append(er2)
    }
    
    var count: Int {
        return reports.count
    }
    
    subscript(_ index: Int) -> ExpenseReport {
        get {
            return reports[index]
        }
    }
    
    func addOrUpdate(_ expenseReport: ExpenseReport?) {
        guard let reportToAdd = expenseReport else {
            return // Simply ignore nil
        }
        
        self.update {
            addIfNotPresent(reportToAdd)
        }
    }
    
    func delete(_ expenseReport: ExpenseReport?) {
        guard let reportToDelete = expenseReport else {
            return // Simply ignore nil
        }
        
        self.update {
            if let index = reports.index(where: { $0.id == reportToDelete.id }) {
                reports.remove(objectAtIndex: index)
            }
        }
    }
    
    private func addIfNotPresent(_ reportToAdd: ExpenseReport) {
        let report = reports.first(where: { $0.id == reportToAdd.id })
        if report == nil {
            reports.append(reportToAdd)
        }
    }
}
