import Foundation
import RealmSwift

extension Object {
    func update(_ action: () -> ()) {
        if let realm = realm {
            try! realm.write {
                action()
            }
        }
        else {
            action()
        }
    }
}
