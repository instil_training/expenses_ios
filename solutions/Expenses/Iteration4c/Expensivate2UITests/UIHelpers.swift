import Foundation
import XCTest

class ExpenseReportTableCellUI {
    let element: XCUIElement
    
    init(_ element: XCUIElement) {
        self.element = element
    }
    
    var title: XCUIElement {
        return element.staticTexts["titleLabel"]
    }
    
    var cost: XCUIElement {
        return element.staticTexts["costLabel"]
    }
}

class ApplicationUI {
    var app = XCUIApplication()
    
    func expenseReportRow(index: Int) -> ExpenseReportTableCellUI {
        return ExpenseReportTableCellUI(tableRows[index])
    }
    
    var lastExpenseReportRow: ExpenseReportTableCellUI {
        return expenseReportRow(index: tableRows.count - 1)
    }
    
    var tableRows: [XCUIElement] {
        return app.tables.cells.allElementsBoundByIndex
    }
    
    var title: XCUIElement {
        return app.textFields["titleText"]
    }

    var cost: XCUIElement {
        return app.textFields["costText"]
    }
    
    var save: XCUIElement {
        return app.navigationBars.buttons["Save"]
    }
    
    var cancel: XCUIElement {
        return app.navigationBars.buttons["Cancel"]
    }

    var addNewReport: XCUIElement {
        return app.navigationBars.buttons["Add Report"]
    }

    var addNewExpense: XCUIElement {
        return app.buttons["addExpenseButton"]
    }
    
    func tapRow(index: Int) {
        app.tables.cells.allElementsBoundByIndex[index].tap()
    }
}

extension XCUIElement {
    func clearText() {
        self.tap()
        if let size = (self.value as? String)?.characters.count {
            self.typeText(String(repeating: "\u{8}", count: size))
        }
    }
    
    var text: String {
        get {
            return self.value as! String
        }
        
        set {
            clearText()
            self.typeText(newValue)
        }
    }
}
