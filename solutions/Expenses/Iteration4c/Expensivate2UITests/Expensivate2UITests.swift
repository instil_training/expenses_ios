import XCTest

class Expensivate2UITests: XCTestCase {
    var app: XCUIApplication!
    var ui: ApplicationUI!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        app = XCUIApplication()
        
        ui = ApplicationUI()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // *** These two tests use the UI abstractions
    //     and helpers above to make writing the tests easier
    func testEditingTitles() {
        let testCases = [0, 1]
        
        testCases.forEach({ index in
            let expectedTitle = "DummyTitle \(index)"
            ui.tableRows[index].tap()
            ui.title.text = expectedTitle
            
            ui.save.tap()
            
            let titleOnRow = ui.expenseReportRow(index: index).title
            XCTAssertEqual(expectedTitle, titleOnRow.label)
        })
    }

    func testCancelEditingDoesNotUpdateTitle() {
        ui.tableRows[1].tap()
        let originalTitle = ui.title.text
        ui.title.text = "Should Not Update"
        
        ui.cancel.tap()
        
        let titleOnRow = ui.expenseReportRow(index: 1).title
        XCTAssertEqual(originalTitle, titleOnRow.label)
    }
    
    // **** This test goes further and actually uses BDD Steps
    func testAddNewExpenseReportWorks() {
        given {
            userAddsANewReport()
            userSetsTheTitleTo("A New Report")
            userAddsANewExpense(title: "Taxi", cost: 10.0)
            userAddsANewExpense(title: "Food", cost: 30.0)
        }
        when {
            userSaves()
        }
        then {
            theLastExpenseReportShows(title: "A New Report", cost: 40.0)
        }
    }
}
