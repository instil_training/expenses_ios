import Foundation
import XCTest

func given(_ action: () -> ()) {
    action()
}

let when = given
let then = given

let ui = ApplicationUI()

func userAddsANewReport() {
    ui.addNewReport.tap()
}

func userSetsTheTitleTo(_ title: String) {
    ui.title.text = title
}

func userAddsANewExpense(title: String, cost: Double) {
    ui.addNewExpense.tap()
    ui.title.text = title
    ui.cost.text = String(cost)
    ui.save.tap()
}

func userSaves() {
    ui.save.tap()
}

func theLastExpenseReportShows(title: String, cost: Double) {
    let lastRow = ui.lastExpenseReportRow
    
    XCTAssertEqual(title, lastRow.title.label)
    XCTAssertEqual("£\(cost)", lastRow.cost.label)
}

