import Foundation
import RealmSwift

class ExpenseReportService {
    private let realm: Realm
    let repository: ExpenseReportRepository
    
    init() {
        realm = try! Realm()
        
        print("Initialising")
        if let repository = realm.objects(ExpenseReportRepository.self).first {
            self.repository = repository
        }
        else {
            print("No object found")
            self.repository = ExpenseReportService.createRepository()
            try! realm.write {
                print("Adding repository")
                repository.populateWithDummyData()
                realm.add(self.repository)
                print("Added")
            }
        }
    }
    
    static func createRepository() -> ExpenseReportRepository {
        let repository = ExpenseReportRepository()
        return repository
    }
}



