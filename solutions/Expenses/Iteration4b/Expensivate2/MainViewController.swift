import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let repository = ExpenseReportService().repository
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "expenseReportCell", for: indexPath) as! ExpenseReportTableViewCell
        cell.expenseReport = repository[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repository.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard var destination = segue.destination as? ExpenseReportHolder,
              let identifier = segue.identifier else {
            return
        }
        
        switch identifier {
            case "editExpenseReport":
                guard let sender = sender as? ExpenseReportHolder else {
                    return
                }
                destination.expenseReport = sender.expenseReport
            case "addExpenseReport":
                destination.expenseReport = ExpenseReport()
            default:
                print("Unknown segue")
                break
        }
    }
    
    @IBAction func saveChanges(_ segue: UIStoryboardSegue) {
        guard let source = segue.source as? Saver,
              let sourceExpenseReportHolder = source as? ExpenseReportHolder else {
            print("Returned from non Expense Holder")
            return
        }
        
        source.saveChanges()
        repository.addOrUpdate(sourceExpenseReportHolder.expenseReport)
        tableView.reloadData()
    }
    
    @IBAction func cancel(_ segue: UIStoryboardSegue) {
        tableView.reloadData()
    }
    
    @IBAction func deleteReport(_ segue: UIStoryboardSegue) {
        guard let expenseReportHolder = segue.source as? ExpenseReportHolder else {
            print("Returned from non Expense Holder")
            return
        }
        
        repository.delete(expenseReportHolder.expenseReport)
        tableView.reloadData()
    }
}
