import Foundation
import RealmSwift

class Expense: Object {
    dynamic var id = UUID().uuidString
    dynamic var title = ""
    dynamic var date = ""
    dynamic var cost = 0.0
        
    override var description: String {
        return "\(title) - \(cost)"
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Expense {
    func with(title: String) -> Expense {
        self.title = title
        return self
    }
    
    func with(date: String) -> Expense {
        self.date = date
        return self
    }
    
    func with(cost: Double) -> Expense {
        self.cost = cost
        return self
    }
}
