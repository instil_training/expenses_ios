import UIKit

class ExpenseTableViewCell: UITableViewCell, ExpenseHolder {

    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var expense: Expense? {
        didSet {
            if let currentExpense = expense {
                titleLabel.text = currentExpense.title
                dateLabel.text = currentExpense.date
                costLabel.text = "£\(currentExpense.cost)"
            }
        }
    }
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }    
}
