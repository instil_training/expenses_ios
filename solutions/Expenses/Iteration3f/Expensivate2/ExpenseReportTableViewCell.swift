import UIKit

class ExpenseReportTableViewCell: UITableViewCell, ExpenseReportHolder {

    @IBOutlet weak var expenseCountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var totalCostLabel: UILabel!
    
    var expenseReport: ExpenseReport? {
        didSet {
            if let report = expenseReport {
                titleLabel.text = report.title
                expenseCountLabel.text = "\(report.expenses.count)"
                totalCostLabel.text = "£\(report.totalCost)"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
