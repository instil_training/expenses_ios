import Foundation

protocol Saver {
    func saveChanges()
}
