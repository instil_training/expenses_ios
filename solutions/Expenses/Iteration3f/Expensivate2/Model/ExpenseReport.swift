import Foundation

class ExpenseReport : CustomStringConvertible {
    var expenses: [Expense] = []
    
    var date: String
    var title: String
    
    init(title: String) {
        self.title = title
        self.date = ""
    }
    
    var description: String {
        return "\(title) (\(expenses.count) expenses)"
    }
    
    var totalCost: Double {
        return expenses.reduce(0.0, { $0 + $1.cost })
    }
    
    func addOrUpdate(_ expense: Expense?) {
        guard let expenseToAdd = expense else {
            return // Simply ignore nil
        }
        
        addIfNotPresent(expenseToAdd)
        // No update require as using reference
    }
    
    func addIfNotPresent(_ expenseToAdd: Expense) {
        let expense = expenses.first(where: { $0 === expenseToAdd })
        if expense == nil {
            expenses.append(expenseToAdd)
        }
    }
    
    func delete(_ expense: Expense?) {
        guard let expenseToDelete = expense else {
            return
        }
        
        if let index = expenses.index(where: { $0 === expenseToDelete }) {
            expenses.remove(at: index)
        }
    }
}
