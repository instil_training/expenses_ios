import Foundation

class Expense: CustomStringConvertible {
    var title = ""
    var date = ""
    var cost = 0.0
    
    init() {
    }
    
    init(title: String, date: String, cost: Double) {
        self.title = title
        self.date = date
        self.cost = cost
    }
    
    var description: String {
        return "\(title) - \(cost)"
    }
}
