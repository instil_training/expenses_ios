/*: optional types; unwrapping optionals; guards
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Optionals
 */

//: ## Declaring optional types


//: ## Unwrapping

//: ### Forced Unwrapping

//: ### Nil-coalescing


//: ### Optional Binding


//: ### Guards



//: ### Optional Chaining


//: ## Binding Behaviour


//: ## Implicit Optionals


//: [Previous](@previous) - [Next](@next)
