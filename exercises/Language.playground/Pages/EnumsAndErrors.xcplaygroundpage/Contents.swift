/*: lambdas; defining blocks; passing blocks as arguments;
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Enums and Error handling
 */

//: ## Enums

//: ## Enums with associated values

//: ## Raw Value Enums

//: ## Defining errors

//: ## Throwing errors

//: ## Executing Throwable Code

//: ### Catching Errors

//: ## Defer

//: ## Assertions


//: [Previous](@previous) - [Next](@next)
