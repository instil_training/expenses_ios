/*: Control Flow
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Control Flow
 */
import Foundation

//: ## If Block


//: ## For loop

//: ## While Loop


//: ## Repeat While


//: ## Switch Statements


//: [Next](@next)
