/*: defining classes; initialising instances; structs; enums
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Classes
 */

import Foundation

//: ## Defining classes and structures



//: ## Copy by value or reference


//: ## Properties


//: ## Property Observers



//: ## Subscripts



//: ## Extending existing classes


//: ## Failable Initialisers


//: Type Checks and Casts

//: [Previous](@previous) - [Next](@next)
