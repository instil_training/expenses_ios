/*: lambdas; defining blocks; passing blocks as arguments;
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Closures
 */

import Foundation

//: ## Higher Order Functions
//: ### Returning functions
func powers(of base: Int) -> (Int) -> Int {
    func raiseBase(to: Int) -> Int {
        return Int(pow(Double(base), Double(to)))
    }
    
    return raiseBase;
}
print(type(of: powers))

let powersOf2 = powers(of: 2)
print(powersOf2(8))

//: ### Functions as input and output


//: ## Defining closures expressions

//: ## Trailing closures
func time(_ action: () -> ()) {
    let startTime = CFAbsoluteTimeGetCurrent()
    action()
    let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
    print("Time elapsed: \(timeElapsed) seconds")
}

time {
    let end = 10_000
    var result: Int64 = 0
    for number in 1...end {
        result += result
    }
    print("Sum of first \(end) numbers is \(result)")
}

//: ## Auto-closures
func multiply(_ a: Int, _ b: Int) -> Int {
    print("**** Multiplying values")
    return a * b
}

func deferEvaluation(_ input: @autoclosure () -> Int) {
    print("Entering function")
    print("Was passed in the value \(input())")
    print("Exiting function")
}

deferEvaluation(2)
deferEvaluation(multiply(10, 123))


//: ### Type aliases
typealias IntPredicate = (Int) -> Bool
var isEven4: IntPredicate = { $0 % 2 == 0 }
print(isEven4(15))

typealias StraightTransform<T> = (T) -> T

//: ## Operators as Functions
func mathApplier(_ a: Int, _ b: Int, operation: (Int, Int) -> Int) -> Int {
    return operation(a, b)
}

print(mathApplier(10, 3, operation:  +))
print(mathApplier(10, 3, operation:  -))
print(mathApplier(10, 3, operation:  *))
print(mathApplier(10, 3, operation:  /))

print( [6, 23, 54, 123, 12, 1].sorted(by: >) )

//: [Previous](@previous) - [Next](@next)
