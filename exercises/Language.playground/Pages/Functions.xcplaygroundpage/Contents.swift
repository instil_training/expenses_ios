/*: defining and calling functions
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Functions
 */

import Foundation

//: ## Defining functions



//: ## Argument labels



//: ## Overloads via Labels


//: ## Default arguments


//: ## Variadic Functions


//: ## Inout Arguments


//: ## Nested Functions
func CollatzSequence(start: Int) -> [Int] {
    // This variable must be before the function
    var sequence: [Int] = []
    
    func even(_ input: Int) -> Int{
        return input / 2
    }
    
    func odd(_ input: Int) -> Int {
        return (input * 3) + 1
    }
    
    func populate(_ input: Int) {
        sequence.append(input)
        switch input {
        case 1:  return
        case let x where x % 2 == 0:
            populate(even(input))
        default:
            populate(odd(input))
        }
    }
    
    // The function usage must be after definition
    populate(start)
    return sequence
}

print(CollatzSequence(start: 12))

//: [Previous](@previous) - [Next](@next)
