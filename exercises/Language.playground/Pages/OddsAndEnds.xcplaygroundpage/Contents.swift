/*: Odds and Ends;
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Odds and Ends
 */

import Foundation

//: ## Weak References and Deinitialiser
class Dummy {
    init?() {}
    
    deinit {
        print("Deinitialiser Called")
    }
}

var p1 = Dummy()
var p2 = p1
weak var p3 = p2

print(p3)
p1 = nil
print(p3)
p2 = nil   // p3 made nil too
print(p3)

//: [Next](@next)
