/*: declaring variables; type inference
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Variables
 */

//: ## Types - Inferred and Explicit


//: ## Unicode Naming
var 🍿🤗💩 = "Never do this"
var クレイジーネーム = 123
🍿🤗💩 += "...no seriously. Never do this!"


//: ## Basic Operations


//: ### Collection Operations


//: ### Decompose Tuple

//: ## Constants vs Variables

//: ## Casting

//: ## Ranges

//: [Previous](@previous) - [Next](@next)
