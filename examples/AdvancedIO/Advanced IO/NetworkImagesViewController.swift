import UIKit

let hostUrl = URL(string: "http://localhost:8080")!
let imagesUrl = hostUrl.appendingPathComponent("images")

func readJsonArray(fromData data: Data) -> [ImageDescriptor] {
    var json: Any?
    do {
        json = try JSONSerialization.jsonObject(with: data)
    } catch {
        print(error)
    }

    guard let array = json as? [[String: Any]] else {
        return []
    }
    
    print("Read \(array.count) records")
    return array.flatMap({ ImageDescriptor(json: $0) })
    
}

class NetworkImagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    private var imageDescriptors: [ImageDescriptor] = []
    private let urlSession = URLSession(configuration: .default)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshImages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageDescriptors.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let image = imageDescriptors[indexPath.row]
        let cell = UITableViewCell()
        cell.textLabel?.text = image.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let imageDescriptor = imageDescriptors[indexPath.row]
        let imagePath = URL(string: imageDescriptor.link)!
        print("Reading \(imagePath.absoluteString)")
        let task = urlSession.dataTask(with: imagePath) { data, response, error in
            DispatchQueue.main.async {
                if let data = data {
                    let image = UIImage(data: data)
                    self.imageView.image = image
                }
            }
        }
        task.resume()
    }
    
    @IBAction func refresh(_ sender: Any) {
        refreshImages()
    }
    
    func refreshImages() {
        refreshButton.isEnabled = false
        print("Reading data from REST API - " + imagesUrl.absoluteString)
        
        let task = urlSession.dataTask(with: imagesUrl) { data, response, error in
            if let data = data {
                self.imageDescriptors = readJsonArray(fromData: data)
                print("Got back \(self.imageDescriptors.count) image descriptors")
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.refreshButton.isEnabled = true
            }
        }
        task.resume()
    }
    
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func sendToServer(_ sender: Any) {
        guard let currentImage = imageView.image else {
            return
        }
        
        let url = imagesUrl.appendingPathComponent("\(imageDescriptors.count).jpg")
        print("Sending \(url.absoluteString)")
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let imageData = UIImageJPEGRepresentation(currentImage,  0.9)
        request.httpBody = imageData
        
        let task = urlSession.dataTask(with: request) { data, response, error in
            let httpResponse = response as? HTTPURLResponse
            print("Task completed: \(httpResponse?.statusCode ?? -1)")
            
            DispatchQueue.main.async {
                self.refreshImages()
            }
        }
        task.resume()
    }
}
