import UIKit

class StudentsViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var studentTable: UITableView!

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Number of items = ", students.count)
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("Generating cell ", indexPath)
        let cell = studentTable.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath) as! StudentTableViewCell
        cell.student = students[indexPath.row]
        return cell
    }

}
