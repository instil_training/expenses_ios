import Foundation

struct Sequence {
    let title: String
    let values: [Int]
}

let sequences = [
    Sequence(title: "Fibonacci",
             values: [1, 2, 3, 5, 8, 13, 21]),
    Sequence(title: "Squares",
             values: [1, 4, 9, 25, 36, 49]),
    Sequence(title: "Triangular",
             values: [1, 3, 6, 10, 15, 21])
]

