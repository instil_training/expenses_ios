import Foundation

struct Student {
    let name: String
    let subject: String
    let year: Int
}

let students = [
    Student(
        name: "Bob Bobson",
        subject: "Computer Science",
        year: 3),
    Student(
        name: "Sally Sallerson",
        subject: "Mathematics",
        year: 2),
    Student(
        name: "John Johnson",
        subject: "Philosophy",
        year: 4)
]
