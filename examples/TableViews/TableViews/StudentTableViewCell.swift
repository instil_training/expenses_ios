import UIKit

class StudentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    var student: Student? {
        didSet {
            if let newValue = student {
                nameLabel.text = newValue.name
                subjectLabel.text = newValue.subject
                yearLabel.text = String(newValue.year)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
