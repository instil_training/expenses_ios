import Foundation

struct ImageDescriptor {
    var name: String
    var link: String
    
    init?(json: [String: Any]) {
        guard let name = json["name"] as? String,
              let link = json["link"] as? String else {
            return nil
        }
        
        self.name = name
        self.link = link
    }
}
