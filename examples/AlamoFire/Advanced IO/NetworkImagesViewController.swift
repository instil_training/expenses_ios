import UIKit
import Alamofire
import AlamofireImage

let hostUrl = URL(string: "http://localhost:8080")!
let imagesUrl = hostUrl.appendingPathComponent("images")

func readArray(fromJson json: Any?) -> [ImageDescriptor]? {
    guard let array = json as? [[String: Any]] else {
        return nil
    }
    
    print("Read \(array.count) records")
    return array.flatMap({ ImageDescriptor(json: $0) })
    
}

class NetworkImagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    private var imageDescriptors: [ImageDescriptor] = []
    private let urlSession = URLSession(configuration: .default)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshImages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageDescriptors.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let image = imageDescriptors[indexPath.row]
        let cell = UITableViewCell()
        cell.textLabel?.text = image.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let imageDescriptor = imageDescriptors[indexPath.row]
        let imagePath = URL(string: imageDescriptor.link)!
        print("Reading \(imagePath.absoluteString)")
        
        Alamofire.request(imagePath)
                 .validate()
                 .validate(contentType: ["image/jpeg"])
                 .responseImage { response in
            switch response.result {
            case .success(let image):
                self.imageView.image = image
            case .failure(let error):
                print("Error doing HTTP: \(error)")
            }
        }
    }
    
    @IBAction func refresh(_ sender: Any) {
        refreshImagesValidation()
    }
    
    func refreshImages() {
        refreshButton.isEnabled = false
        print("Reading data from REST API - " + imagesUrl.absoluteString)

        Alamofire.request(imagesUrl).responseJSON { response in
            defer {
                self.refreshButton.isEnabled = true
            }
            
            // Process response.result.value
            guard let array = response.result.value as? [[String: Any]] else {
                return
            }

            let images = array.flatMap({ ImageDescriptor(json: $0) })
            self.imageDescriptors = images
            
            self.tableView.reloadData()
        }
    }
    
    func refreshImagesValidation() {
        refreshButton.isEnabled = false
        print("Reading data from REST API - " + imagesUrl.absoluteString)
        
        Alamofire.request(imagesUrl)
                 .validate()
                 .responseJSON { response in
            switch response.result {
            case .success(let json):
                if let array = json as? [[String: Any]] {
                    let images = array.flatMap({ ImageDescriptor(json: $0) })
                    self.imageDescriptors = images
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print("Error doing HTTP: \(error)")
            }
                    
            self.refreshButton.isEnabled = true
        }
    }
    
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    
}
