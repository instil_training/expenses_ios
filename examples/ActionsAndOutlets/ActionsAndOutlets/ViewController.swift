import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var decrementButton: UIButton!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var incrementButton: UIButton!
    
    private let counter = Counter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        refreshView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func incrementCounter(_ sender: Any) {
        counter.increment()
        refreshView()
    }
    
    @IBAction func decrementCounter(_ sender: UIButton) {
        counter.decrement()
        refreshView()
    }

    func refreshView() {
        counterLabel.text = String(counter.value)
        decrementButton.isEnabled = counter.canDecrement
        incrementButton.isEnabled = counter.canIncrement
    }
}

