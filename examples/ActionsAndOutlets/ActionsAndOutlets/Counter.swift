import Foundation

class Counter {
    let MAX_COUNT = 10
    private var _value = 0
    
    var value: Int {
        return _value
    }
    
    var canIncrement: Bool {
        return _value < MAX_COUNT
    }
    
    var canDecrement: Bool {
        return _value > 0
    }
    
    func increment() {
        if canIncrement {
            _value += 1
        }
    }
    
    func decrement() {
        if canDecrement {
            _value -= 1
        }
    }
}
