import Foundation

func given(_ action: () -> Void) {
    action()
}

let when = given
let then = given
