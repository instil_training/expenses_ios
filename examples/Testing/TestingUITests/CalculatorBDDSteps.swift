import Foundation
import XCTest

let app = XCUIApplication()
let ui = CalculatorUI(app: app)

func theUserEntersTheOperands(_ lhs: Double, _ rhs: Double) {
    ui.input1.setText(String(lhs))
    ui.input2.setText(String(rhs))
}

func theUserClicksTheDivideButton() {
    ui.divide.tap()
}

func theResultIs(_ result: Double) {
    XCTAssertEqual(String(result), ui.result.text)
}

