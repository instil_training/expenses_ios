import XCTest

class CalculatorUITests: XCTestCase {
    
    var app: XCUIApplication!
    var ui: CalculatorUI!
    
    override func setUp() {
        super.setUp()
        
        XCUIApplication().terminate()
        
        continueAfterFailure = false

        XCUIApplication().launch()
        
        app = XCUIApplication()
        ui = CalculatorUI(app: app)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddingTwoNumbersTogether() {
        let input1 = app.textFields["input1"]
        input1.tap()
        input1.typeText("30")
        
        let input2 = app.textFields["input2"]
        input2.tap()
        input2.typeText("20")
        
        let plusButton = app.buttons["plusButton"]
        plusButton.tap()
        
        let result = app.textFields["result"]
        
        XCTAssertEqual("50.0", result.value as! String)
    }
    
    func testSubtractTwoNumbers() {
        ui.input1.setText("30")
        ui.input2.setText("20")
        
        ui.subtract.tap()
        
        XCTAssertEqual("10.0", ui.result.text)
    }

    func testMultiplyTwoNumbers() {
        ui.input1.setText("30")
        ui.input2.setText("20")
        
        ui.multiply.tap()
        
        XCTAssertEqual("600.0", ui.result.text)
    }
    
    func testDivideTwoNumbers() {
        given {
            theUserEntersTheOperands(40, 20)
        }
        when {
            theUserClicksTheDivideButton()
        }
        then {
            theResultIs(2)
        }
    }
}
