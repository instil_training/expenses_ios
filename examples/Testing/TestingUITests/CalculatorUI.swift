import Foundation
import XCTest

class CalculatorUI {
    let app: XCUIApplication
    
    init(app: XCUIApplication) {
        self.app = app
    }
    
    var input1: XCUIElement {
        return app.textFields["input1"]
    }
    
    var input2: XCUIElement {
        return app.textFields["input2"]
    }
    
    var result: XCUIElement {
        return app.textFields["result"]
    }
    
    var plus: XCUIElement {
        return app.buttons["plusButton"]
    }
    
    var subtract: XCUIElement {
        return app.buttons["subtractButton"]
    }
    
    var multiply: XCUIElement {
        return app.buttons["multiplyButton"]
    }
    
    var divide: XCUIElement {
        return app.buttons["divideButton"]
    }
}
