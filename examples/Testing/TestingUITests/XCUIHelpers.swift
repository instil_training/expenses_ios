import Foundation
import XCTest

extension XCUIElement {
    func setText(_ text: String) {
        self.tap()
        self.typeText(text)
    }
    
    var text: String {
        return self.value as! String
    }
}
