import UIKit

class ViewController: UIViewController {

    let calculationService = CalculationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        resetError()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func resetError() {
        setError("")
    }
    
    @IBAction func calculation(_ sender: UIButton) {
        switch sender.titleLabel?.text ?? "Unknown" {
        case "+":
            calculate(operation: calculationService.add)
        case "-":
            calculate(operation: calculationService.subtract)
        case "*":
            calculate(operation: calculationService.multiply)
        case "/":
            calculate(operation: calculationService.divide)
        default:
            setError("Unrecognised operation")
        }
    }

    func setError(_ error: String) {
        errorLabel.text = error
    }
    
    func calculate(operation: (Double, Double) -> Double) {
        if let (lhs, rhs) = getOperands() {
            successfulCalculation(operation(lhs, rhs))
        }
    }
    
    func successfulCalculation(_ result: Double) {
        resultText.text = String(result)
        resetError()
    }
    
    func calculate(operation: (Double, Double) throws -> Double) {
        if let (lhs, rhs) = getOperands() {
            do {
                let result = try operation(lhs, rhs)
                successfulCalculation(result)
            } catch CalculationError.divideByZero {
                setError("Divide by zero error")
            } catch {
                setError("Error calculating")
            }
        }
    }
    
    func getOperands() -> (Double, Double)? {
        guard let lhs = Double(input1Text.text!),
              let rhs = Double(input2Text.text!) else {
            setError("Invalid inputs")
            return nil
        }
        
        return (lhs, rhs)
    }
    
    @IBOutlet weak var input1Text: UITextField!
    @IBOutlet weak var input2Text: UITextField!
    @IBOutlet weak var resultText: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
}

