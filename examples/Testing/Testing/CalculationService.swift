import Foundation

enum CalculationError : Error {
    case divideByZero
}

class CalculationService {
    func add(_ lhs: Double, _ rhs: Double) -> Double {
        return lhs + rhs
    }
    
    func subtract(_ lhs: Double, _ rhs: Double) -> Double {
        return lhs - rhs
    }
    
    func multiply(_ lhs: Double, _ rhs: Double) -> Double {
        return lhs * rhs
    }
    
    func divide(_ lhs: Double, _ rhs: Double) throws -> Double {
        guard rhs != 0.0 else {
            throw CalculationError.divideByZero
        }
        
        return lhs / rhs
    }
}
