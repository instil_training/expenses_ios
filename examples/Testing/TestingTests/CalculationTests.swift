import XCTest
@testable import Testing

typealias CalculationTestCase = (
    name: String,
    lhs: Double,
    rhs: Double,
    result: Double
)

class CalclationTests : XCTestCase {
    
    var target = CalculationService()
    
    override func setUp() {
        super.setUp()
        print("Setting up test")
    }
    
    override func tearDown() {
        print("Tearing test down")
        super.tearDown()
    }
    
    func testAddingTwoNumbersTogether() {
        let result = target.add(1, 2)
        
        XCTAssertEqual(3, result)
    }

    func testSubtractingTwoNumbersTogether() {
        let cases: [CalculationTestCase] = [
            ("Simple 1", 1, 0, 1),
            ("Simple 1 Reversed", 0, 1, -1),
            ("Simple 2", 7, 5, 2),
            ("Simple 2 Reversed", 5, 7, -2)
        ]
        
        cases.forEach { testCase in
            let result = target.subtract(testCase.lhs, testCase.rhs)
        
            XCTAssertEqual(result, testCase.result, "\(testCase) failed")
        }
    }

    func testMultiplyingTwoNumbersTogether() {
        let cases: [CalculationTestCase] = [
            ("Simple 1", 1, 0, 0),
            ("Simple 1 Reversed - Communative", 0, 1, 0),
            ("Simple 2", 7, 5, 35),
            ("Simple 2 Reversed - Communative", 5, 7, 35)
        ]
        
        cases.forEach { testCase in
            let result = target.multiply(testCase.lhs, testCase.rhs)
            
            XCTAssertEqual(result, testCase.result, "\(testCase) failed")
        }
    }

    func testDividingTwoNumbers() throws {
        let cases: [CalculationTestCase] = [
            ("Simple 1", 2, 1, 2),
            ("Simple 1 Reversed", 1, 2, 0.5),
            ("Simple 2", 40, 10, 4),
            ("Simple 2 Reversed - Communative", 10, 40, 0.25)
        ]
        
        cases.forEach { testCase in
            let result = try! target.divide(testCase.lhs, testCase.rhs)
            
            XCTAssertEqual(result, testCase.result, "\(testCase) failed")
        }
    }

    func testDivideByZeroThrowsError() throws {
        XCTAssertThrowsError(try target.divide(1, 0))
    }
    
    func testPerformanceOfSlowCode() {
        self.measure {
            sleep(1)
        }
    }
}
