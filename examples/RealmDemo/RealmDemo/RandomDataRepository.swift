import Foundation
import RealmSwift

class RandomData : Object {
    dynamic var integer = Int(arc4random_uniform(100))
    dynamic var double = Double(arc4random_uniform(100))
    
    override var description: String {
        return "\(integer) - \(double)"
    }
}

class RandomDataRepository : Object {
    let data = List<RandomData>()
}

class RandomDataService {
    let realm: Realm
    let repository: RandomDataRepository
    
    init() {
        realm = try! Realm()
        
        if let repository = realm.objects(RandomDataRepository.self).first {
            self.repository = repository
        }
        else {
            self.repository = RandomDataService.createNewRepository(realm)
        }
    }
    
    var count: Int {
        return repository.data.count
    }
    
    func add() {
        try! realm.write {
            repository.data.append(RandomData())
        }
    }
    
    func pop() {
        try! realm.write {
            repository.data.removeLast()
        }
    }
    
    func clear() {
        try! realm.write {
            repository.data.removeAll()
        }
    }
    
    func get(_ index: Int) -> RandomData {
        return repository.data[index]
    }
    
    class func createNewRepository(_ realm: Realm) -> RandomDataRepository {
        let repository = RandomDataRepository()
        try! realm.write {
            realm.add(repository)
        }
        
        return repository
    }
}

//struct RandomData : CustomStringConvertible {
//    let integer: Int
//    let uuid: UUID
//    
//    init() {
//        integer = Int(arc4random_uniform(100))
//        uuid = UUID()
//    }
//    
//    var description: String {
//        return "\(integer) - \(uuid)"
//    }
//}
//
//class RandomDataRepository {
//    private var _data: [RandomData] = []
//    
//    var data: Array<RandomData> {
//        return _data
//    }
//    
//    var count: Int {
//        return _data.count
//    }
//    
//    func add() {
//        _data.append(RandomData())
//    }
//    
//    func get(_ index: Int) -> RandomData {
//        return _data[index]
//    }
//}
