import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countLabel: UILabel!

    let service = RandomDataService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return service.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = service.get(indexPath.row).description
        return cell
    }
    
    func refreshData() {
        countLabel.text = String(service.count)
        tableView.reloadData()
    }
    
    @IBAction func addNewData(_ sender: Any) {
        service.add()
        refreshData()
    }
    
    @IBAction func popData(_ sender: Any) {
        service.pop()
        refreshData()
    }
    
    @IBAction func clearData(_ sender: Any) {
        service.clear()
        refreshData()
    }
}

