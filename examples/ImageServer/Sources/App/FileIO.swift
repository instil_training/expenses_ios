import Foundation

let fileManager = FileManager.default

func getFileList(at: String) -> [String] {
    do {
        let directoryContents = try fileManager.contentsOfDirectory(atPath: at)
        print(directoryContents)
        return directoryContents
    } catch let error as NSError {
        print(error.localizedDescription)
        return ["No data found", error.localizedDescription]
    }
}

func readFile(_ path: String) -> Bytes? {
    print("Reading file: \(path)")
    print("File exists \(fileManager.fileExists(atPath: path))")
    
    let data = fileManager.contents(atPath: path)
    return data?.makeBytes()
}

func writeFile(_ path: String, data: Bytes) {
    print("Writing file: \(path)")
    fileManager.createFile(atPath: path, contents: Data(data), attributes: nil)
}
