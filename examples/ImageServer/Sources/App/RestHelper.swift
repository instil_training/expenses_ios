import Foundation
import Vapor

extension Response {
    class func image(fromBytes fileContents: Bytes) -> Response {
        let response = Response(status: .ok, body: .data(fileContents))
        response.headers["Content-type"] = "image/jpeg"
        return response
    }
    
    class func image(fromFilePath filePath: String) -> Response {
        if let fileContents = readFile(filePath) {
            return Response.image(fromBytes: fileContents)
        }
        
        return Response(status: .notFound, body: "Unknown image path: \(filePath)")
    }
    
    class func html(_ body: String) -> Response {
        let response = Response(status: .ok, body: body)
        response.headers["Content-type"] = "text/html"
        return response
    }
}
