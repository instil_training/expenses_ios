import Foundation
import Vapor

struct ImageDescriptor {
    let name: String
    let link: String
}

extension ImageDescriptor : JSONRepresentable {
    func makeJSON() throws -> JSON {
        return try JSON(node: [
            "name" : self.name,
            "link" : self.link
            ])
    }
}
