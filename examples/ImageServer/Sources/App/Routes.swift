import Vapor
import Foundation

let ImagePath = "./images"

func getImageLink(req: Request, filename: String) -> String {
    var host = "http://\(req.uri.hostname)"
    host += req.uri.port == nil ? "" : ":\(req.uri.port!.description)"
    let link = "\(host)/images/\(filename)"
    return link
}

extension Droplet {
    func setupRoutes() throws {
        get("hello") { req in
            var json = JSON()
            try json.set("hello", "world")
            return json
        }

        get("listfiles") { req in
            let path = req.query?["path"]?.string ?? "./files"
            return "Files at: \(path)\n" +
                   getFileList(at: path).joined(separator: "\n")
        }
        
        get("images") { req in
            let files = getFileList(at: ImagePath)
            let filesAsJson = files.map({ ImageDescriptor(name: $0, link: getImageLink(req: req, filename: $0)) })
                                   .map({ try? $0.makeJSON() })
                                   .flatMap({ $0 })
            let json = JSON(filesAsJson)
            return json
        }

        get("images2") { req in
            let files = getFileList(at: ImagePath)
            return try files.makeResponse()
        }
        
        get("images", ":filename") { req in
            let filename = try req.parameters.get("filename") as String
            return Response.image(fromFilePath: "\(ImagePath)/\(filename)")
        }
        
        post("images", ":filename") { req in
            print("Receiving file...")
            let filename = try req.parameters.get("filename") as String
            print(filename)
            writeFile("\(ImagePath)/\(filename)", data: req.body.bytes!)
            
            return Response(status: .ok)
        }

        post("getprice", ":filename") { req in
            print("Receiving file...")
            let filename = try req.parameters.get("filename") as String
            print(filename)
            writeFile("\(ImagePath)/\(filename)", data: req.body.bytes!)
            
            var json = JSON()
            try json.set("price", 14.23)
            return json
        }
        
        get("images", ":filename", "html") { req in
            let filename = try req.parameters.get("filename") as String
            let imagePath = getImageLink(req: req, filename: filename)
            let html = "<img src=\"\(imagePath)\">"
            return Response.html(html)
        }
    }
}

extension Array where Element == String {
    public func makeJSON() throws -> JSON {
        let stringsAsJson = self.map({ JSON(stringLiteral: $0) })
        return JSON(stringsAsJson)
    }
    
    public func makeResponse() throws -> Response {
        return try self.makeJSON().makeResponse()
    }
}
