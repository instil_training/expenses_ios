# Image Server

A basic example of server side Swift. This makes use of the Vapor framework.

It is used by the AdvancedIO and Alamofire examples and the solution for the
Expenses app exercise.

## Installation

- Install Vapor - https://vapor.codes/
- Execute 'vapor build' followed by 'vapor run'
- To generate an Xcode project, use 'vapor xcode'

## Server

The server has the following resources

- GET '/images/' - A list of image files on the server
  - An array is returned with an object containing a name and link to contents
- GET '/images/:filename' - Returns the contents of the specified image file
- POST '/images/:filename' - Uploads an image file to the server
- POST '/getprice/:filename' - Uploads an image file and returns a price. Simulate an OCR service
  - This can be used bny the Expenses exercise

