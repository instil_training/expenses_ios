import Foundation

private var instanceId = 0;

func getUniqueId() -> Int {
    defer {
        instanceId += 1
    }
    
    return instanceId
}
