import UIKit

class Screen3ViewController: UIViewController {

    private var instanceId = getUniqueId()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Screen 3 - Id=", instanceId)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
