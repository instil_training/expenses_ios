import UIKit

class MovieViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? MovieTitleViewController,
              let identifier = segue.identifier else {
            return
        }
        
        switch identifier {
        case "showGoodMovie":
            destination.movieTitle = "The Big Lebowski"
        case "showBadMovie":
            destination.movieTitle = "Transformers 4"
        default:
            return
        }
    }
    
    @IBAction func unwindToSelect(_ segue: UIStoryboardSegue) {
        print("Returned via custom exit action")
    }
}
