import UIKit

class IDTrackerViewController: UIViewController {
    
    @IBOutlet weak var instanceLabel: UILabel!
    
    private var instanceId = getUniqueId()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        instanceLabel?.text = "Instance \(instanceId)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

