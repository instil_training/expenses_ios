import UIKit

class Screen1ViewController: UIViewController {
    
    private var instanceId = getUniqueId()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Screen 1 - Id=", instanceId)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

